const arr = [5, 10, 1, 3, 4, 8, 6];

//map

////////// way1
// function double(x){
//     return x*2;
// }
// const output=arr.map(double);

////////// way2
// const output = arr.map(function double(x) {
//   return x * 2;
// });

/////////// way 3
// const output = arr.map((x)=> {
//     return x * 2;
//   });

/////////Can also be written for single line
// const output = arr.map((x) => x * 2);

//filter

// const output = arr.filter(function greaterThan4(x) {
//   return x > 4;
// });

//reduce

///////////sum of the array
// const output = arr.reduce(function (acc, curr) {
//   acc = acc + curr;
//   return acc;
// },0);

///////max no in the array
// const output = arr.reduce(function (max, curr) {
//   if (curr > max) {
//     max = curr;
//   }
//   return max;
// }, 0);

/////// handling api data
const data = [
  { firstName: "Tony", lastName: "Stark", age: 49 },
  { firstName: "Steve", lastName: "Roger", age: 101 },
  { firstName: "Natasha", lastName: "Romanoff", age: 35 },
  { firstName: "Sam", lastName: "Wilson", age: 45 },
  { firstName: "Peter", lastName: "Parker", age: 16 },
  { firstName: "Dr.Stephen", lastName: "Strange", age: 42 },
  { firstName: "Thor", lastName: "Odinson", age: 1500 },
  { firstName: "Bruce", lastName: "Banner", age: 50 },
  { firstName: "Scott", lastName: "Lang", age: 40 },
  { firstName: "Clint", lastName: "Barton", age: 39 },
];

////////Printing first name and last name/List of full name-map
//const output = data.map((x) => x.firstName + ' ' + x.lastName);

/////////counting the people of same age-reduce
// const output = data.reduce(function (acc, curr) {
//   if (acc[curr.age]) {
//     acc[curr.age] = ++acc[curr.age];
//   } else {
//     acc[curr.age] = 1;
//   }
//   return acc;
// }, {});

///////////first name of people of age less than 45 - filter and map
//const output=data.filter((x)=>x.age<45).map((x)=>x.firstName)

///////////first name of people of age less than 45 - reduce
const output = data.reduce(function (acc, curr) {
  if (curr.age < 45) {
    acc.push(curr.firstName);
  }
  return acc;
}, []);

console.log(output);
