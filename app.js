
const express = require('express');
var bodyParser = require('body-parser')
// loading all models
const app = express();
const server = require('http').createServer(app);
server.listen(process.env.PORT || 6006);
app.use(bodyParser.json());
app.use('/api', require('./routes'));

// Mysql DB connection config file
const mysqldb = require('./config/MysqlDB');
//Credentials
const dbConfig = require('./config/mysqlConfig');
//Initialize Mysql DB
initapp();
/**
 * @author: Anil Kumar Ch
 * @since : 23-09-2021
 * @description:Initialize mysql connection here;
 */
function initapp() {
    mysqldb.createPool(dbConfig.config)
        .then(function () {
            console.log('Connected to MySql Successfully.');
        })
        .catch(function (err) {
            console.error('Error occurred creating database connection pool', err);
            console.log('Exiting process');
            process.exit(0);
        });
}