const EmpService = require('../src/services/emp/EmployeeService');
var mysqldb = require('../config/MysqlDB');

const login = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.login(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  
  const insert = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.insertvalue(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  const deleteemp = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.delete(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };

  const update = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.updatevalue(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  const custview = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.custview(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  const custvieworder = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.custvieworder(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  const viewrow = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.viewrow(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  
  const multitabcols = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.multitabcols(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  module.exports = {
    login,
    insert,
    deleteemp,
    update,
    custview,
    custvieworder,
    viewrow,
    multitabcols
  };
  