
//Retrieve User SQL Native Query
function retrieve() {
    return `select * from college.employee`;
}
module.exports.retrieve = retrieve;

function insert(req) {
    return `INSERT INTO college.student (roll_number,student_name,age) VALUES ('${req.rn}','${req.sn}',${req.age})`;
    
}
module.exports.insert = insert;
/*
"rn":"10AB0134",
"sn":"Chandler Bing",
"age":34
*/

function deleteemp(req) {
    return `DELETE FROM college.student WHERE s_no=${req.sln}`;
}
module.exports.deleteemp = deleteemp;
/*
function update(req) {
    return `UPDATE college.student SET roll_number = '${req.rn}',student_name = '${req.sn}',age=${req.age} WHERE s_no=${req.sln}`;
    
}
module.exports.update = update;
*/

function update(req) {
    return `UPDATE college.student SET roll_number='${req.rn}',student_name='${req.sn}',age=${req.age} WHERE s_no = ${req.sln}`;
}
module.exports.update = update;

/*
"rn":"10AB0134",
"sn":"Chandler Bing",
"age":34,
"sln":119
*/
function custview(req) {
    return `SELECT ${req.col} FROM college.student;`;
}
module.exports.custview = custview;
/*  
"col":"roll_number" 
*/
function custvieworder(req) {
    return `SELECT ${req.col2dis} FROM college.student ORDER BY ${req.col2sort} ${req.order}`;
}
module.exports.custvieworder = custvieworder;
/*  
"col2dis":"roll_number" ,
"col2sort":"age",
"order":"desc"
*/
function viewrow(req) {
    return `SELECT * FROM college.student WHERE s_no = ${req.s_no}`;
}
module.exports.viewrow = viewrow;
/*
"s_no":4
*/
function multitabcols(req) {
    return `SELECT college.student.student_name, college.address.address   
    FROM college.student 
    INNER JOIN college.address 
    ON college.student.s_no = college.address.id  
    ORDER BY college.student.s_no  `;
}
module.exports.multitabcols = multitabcols;