var sqls = require('./employeeSql');

class EmployeeService {
    /**
     * @description Dependency Injection cache and db clients
     * @constructor
     * @param {Object<Mysql>} db
     */

    constructor(db) {
        this.db = db;
    }

    async login(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const updateUser = sqls.update(req);
            const userRes = await this.db.execute(updateUser, '', connection);
            return userRes;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }
    async insertvalue() {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const insertUser = sqls.insert();
            const userRes = await this.db.execute(insertUser, '', connection);
            let resp ={"res":{"s_no":userRes.insertId,msg:"Inserted Successfully"}}
            return resp;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }
    async delete(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const deleteUser = sqls.deleteemp(req);
            const userRes = await this.db.execute(deleteUser, '', connection);
            //return userRes;
            let resp ={"res":{msg:"deleted Successfully"}}
            return resp;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }
    }

    
     async updatevalue(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const updateUser = sqls.update(req);
            const userRes = await this.db.execute(updateUser, '', connection);
            //return userRes;
            let resp ={"res":{"s_no":userRes.insertId,msg:"updated Successfully"}}
            return resp;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }
    async custview(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const viewUser = sqls.custview(req);
            const userRes = await this.db.execute(viewUser, '', connection);
            let obj=[]
            userRes.map((res)=>{
                if(req="roll_number"){
                obj.push(res.roll_number)
                console.log(res.roll_number)
                }
            if(req="s_no"){
                    obj.push(res.s_no)
                    console.log(res.s_no)
                }
            if(req="student_name"){
                        obj.push(res.student_name)
                        console.log(res.student_name)
                }
                if(req="age"){
                    obj.push(res.age)
                    console.log(res.age)
            }
            })
            return{result :obj};
            //let resp ={"res":userRes}
            //return resp ;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }
    async custvieworder(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const viewUser = sqls.custvieworder(req);
            const userRes = await this.db.execute(viewUser, '', connection);
            let obj=[]
            userRes.map((res)=>{
                if(req="roll_number"){
                obj.push(res.roll_number)
                console.log(res.roll_number)
                }
            if(req="s_no"){
                    obj.push(res.s_no)
                    console.log(res.s_no)
                }
            if(req="student_name"){
                        obj.push(res.student_name)
                        console.log(res.student_name)
                }
                if(req="age"){
                    obj.push(res.age)
                    console.log(res.age)
            }
            })
            return{result :obj};
        
            //let resp ={"res":userRes}
            //return resp ;
            //return {"res":userRes.map()}

            //let enumValues = userRes[0];
            //let tempString =enumValues.replace(/\'/g,'');
            //let finalArray = tempString.split(',');
           // return finalArray;
           
          // let obj =[];
    //userRes.map((info)=>{
       // obj.push({"res":userRes})
      // let tempString =info.replace(/\'/g,'');
       //obj = tempString.split(',');
        // obj=finalArray;
   // })

    //return (obj);
    

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }
    async viewrow(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const viewUser = sqls.viewrow(req);
            const userRes = await this.db.execute(viewUser, '', connection);
           // let resp ={"res":userRes}
           // return resp ;
           let obj=[]
            userRes.map((res)=>{
                
            
                    obj.push(res)
                    console.log(res)
               
           
           
            })
            return{result :obj};

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }

    async multitabcols(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const viewUser = sqls.multitabcols(req);
            const userRes = await this.db.execute(viewUser, '', connection);
            let resp ={"res":userRes}
            return resp ;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }

    closeConnect(connection, db) {
        process.nextTick(function () {
            db.releaseConnection(connection);
        });
    }
}
module.exports = EmployeeService;
