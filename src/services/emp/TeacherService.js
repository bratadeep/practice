var sqls = require('./employeeSql');

class TeacherService {
    /**
     * @description Dependency Injection cache and db clients
     * @constructor
     * @param {Object<Mysql>} db
     */

    constructor(db) {
        this.db = db;
    }

    async login(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const updateUser = sqls.update();
            const userRes = await this.db.execute(updateUser, '', connection);
            return userRes;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }
    async insertvalue(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const insertUser = sqls.insert();
            const userRes = await this.db.execute(insertUser, '', connection);
            return userRes;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }
    async delete(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const deleteUser = sqls.deleteemp();
            const userRes = await this.db.execute(deleteUser, '', connection);
            return userRes;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }
    }
     async updatevalue(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const updateUser = sqls.update();
            const userRes = await this.db.execute(updateUser, '', connection);
            return userRes;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }
    closeConnect(connection, db) {
        process.nextTick(function () {
            db.releaseConnection(connection);
        });
    }
}
module.exports = EmployeeService;
