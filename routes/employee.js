var express = require('express');
var router = express.Router();
const empController = require('../controllers/employee.controller');

/**
 * @description: For Login using Mobile Number
 */
router.post('/login', empController.login);
router.post('/insert', empController.insert);
router.post('/delete', empController.deleteemp);
router.post('/update', empController.update);
router.get('/custview', empController.custview);
router.get('/custvieworder', empController.custvieworder);
router.get('/viewrow', empController.viewrow);
router.get('/multitabcols', empController.multitabcols);


module.exports = router;