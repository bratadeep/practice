var async = require("async");
// var stack = [];
// var functionOne = function(callback){
//     //perform actions
//     callback(null,'First Function Result');
//     }
//  var functionTwo = function(callback){
//     //perform actions
//     callback(null,'Second Function Result');
//     }
//  var functionThree = function(callback){
//     //perform actions
//     callback(null,'Third Function Result');
//     }
// stack.push(functionOne);
// stack.push(functionTwo);
// stack.push(functionThree);

// async.parallel(stack,function(err,result){
// console.log(result);
// })

var stack = {};
stack.getUserName = function (callback) {
  var userName = "Bob";
  callback(null, userName);
};
stack.getAge = function (callback) {
  var userAge = 25;
  callback(null, userAge);
};
stack.getGender = function (callback) {
  var Gender = "Male";
  callback(null, Gender);
};

async.parallel(stack, function (err, result) {
  if (err) {
    consoler.err(err);
    return;
  }
  console.log(result);
});
